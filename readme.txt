Steps to run and test:
1. Clone the repo
2. Use 'mvn clean install' to compile the code
3. Step 2 will create a self-executable jar file (the current app packaged with an embedded Tomcat) called "revolut-test-executable.jar" in the target directory.
4. Execute the jar with 'java -jar revolut-test-executable.jar'
5. For testing purposes, on startup, the code adds 2 users, each of them having a bank account in the data storage(simple HashMap).
6. An example money trnsfer request is: 

POST http://localhost:8080/money/transfer
{
	"sender": {
		"userId": 1,
		"bankAccountId": 1
	},
	"recipient": {
		"userId": 2,
		"bankAccountId": 2
	},
	"amount": {
		"amount": 3,
		"currency": "EUR"
	}
}

7. Things that could be improved in the future:
    7.1 Exception handling via Aspects
    7.2 Field validation of the incoming DTOs
    7.3 Usage of a Dependency Injection container