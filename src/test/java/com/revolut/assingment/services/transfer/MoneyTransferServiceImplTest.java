package com.revolut.assingment.services.transfer;

import com.revolut.assingment.dto.transfer.AmountDTO;
import com.revolut.assingment.dto.transfer.RecipientDTO;
import com.revolut.assingment.dto.transfer.SenderDTO;
import com.revolut.assingment.dto.transfer.TransferDTO;
import com.revolut.assingment.exceptions.InsufficientBalanceException;
import com.revolut.assingment.exceptions.UserBlacklistedException;
import com.revolut.assingment.exceptions.ValidationException;
import com.revolut.assingment.model.common.Currency;
import com.revolut.assingment.model.transfer.BankAccountEntity;
import com.revolut.assingment.model.transfer.TransferEntity;
import com.revolut.assingment.model.user.UserEntity;
import com.revolut.assingment.repository.BankAccountRepository;
import com.revolut.assingment.repository.TransferRepository;
import com.revolut.assingment.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class MoneyTransferServiceImplTest {

    private MoneyTransferServiceImpl moneyTransferService;

    private UserRepository userRepository;
    private BankAccountRepository bankAccountRepository;
    private TransferRepository transferRepository;
    private CurrencyConversionService currencyConversionService;
    private TransactionValidationService transactionValidationService;

    @Before
    public void setUp() throws Exception {
        userRepository = mock(UserRepository.class);
        bankAccountRepository = mock(BankAccountRepository.class);
        currencyConversionService = mock(CurrencyConversionService.class);
        transactionValidationService = mock(TransactionValidationService.class);
        transferRepository = mock(TransferRepository.class);

        moneyTransferService = new MoneyTransferServiceImpl(userRepository, bankAccountRepository, currencyConversionService,
                transactionValidationService, transferRepository);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowExceptionWhenTransferDtoNotProvided() throws Exception {
        TransferDTO transferDto = new TransferDTO();

        moneyTransferService.transferMoney(transferDto);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowExceptionWhenSenderNotProvided() throws Exception {
        TransferDTO transferDto = new TransferDTO();

        moneyTransferService.transferMoney(transferDto);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowExceptionWhenSenderUserIdNotProvided() throws Exception {
        TransferDTO transferDto = new TransferDTO();
        SenderDTO sender = new SenderDTO();
        transferDto.setSender(sender);

        moneyTransferService.transferMoney(transferDto);
    }

    @Test(expected = UserBlacklistedException.class)
    public void shouldThrowExceptionWhenSenderIsBlacklisted() throws Exception {
        TransferDTO transferDto = new TransferDTO();
        SenderDTO sender = new SenderDTO();
        sender.setUserId(1l);
        transferDto.setSender(sender);

        UserEntity senderEntity = new UserEntity();
        senderEntity.setBlacklisted(true);

        when(userRepository.findById(1L)).thenReturn(senderEntity);
        doThrow(new UserBlacklistedException()).when(transactionValidationService).validateUserNotBlacklisted(senderEntity);

        moneyTransferService.transferMoney(transferDto);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowExceptionWhenRecipientNotProvided() throws Exception {
        TransferDTO transferDto = new TransferDTO();
        SenderDTO sender = new SenderDTO();
        sender.setUserId(1l);
        transferDto.setSender(sender);

        UserEntity senderEntity = new UserEntity();
        senderEntity.setBlacklisted(false);

        when(userRepository.findById(1L)).thenReturn(senderEntity);

        moneyTransferService.transferMoney(transferDto);
    }

    @Test(expected = UserBlacklistedException.class)
    public void shouldThrowExceptionWhenRecipientIsBlacklisted() throws Exception {
        TransferDTO transferDto = new TransferDTO();
        SenderDTO sender = new SenderDTO();
        sender.setUserId(1l);
        transferDto.setSender(sender);

        UserEntity senderEntity = new UserEntity();
        senderEntity.setBlacklisted(false);

        RecipientDTO recipient = new RecipientDTO();
        recipient.setUserId(2L);
        transferDto.setRecipient(recipient);

        UserEntity recipientEntity = new UserEntity();
        recipientEntity.setBlacklisted(true);

        when(userRepository.findById(1L)).thenReturn(senderEntity);
        when(userRepository.findById(2L)).thenReturn(recipientEntity);
        doThrow(new UserBlacklistedException()).when(transactionValidationService).validateUserNotBlacklisted(recipientEntity);

        moneyTransferService.transferMoney(transferDto);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowExceptionWhenSenderBankAccountNotProvided() throws Exception {
        TransferDTO transferDto = new TransferDTO();
        SenderDTO sender = new SenderDTO();
        sender.setUserId(1l);
        transferDto.setSender(sender);

        UserEntity senderEntity = new UserEntity();
        senderEntity.setBlacklisted(false);

        RecipientDTO recipient = new RecipientDTO();
        recipient.setUserId(2L);
        transferDto.setRecipient(recipient);

        UserEntity recipientEntity = new UserEntity();
        recipientEntity.setBlacklisted(false);

        when(userRepository.findById(1L)).thenReturn(senderEntity);
        when(userRepository.findById(2L)).thenReturn(recipientEntity);

        moneyTransferService.transferMoney(transferDto);
    }

    @Test(expected = InsufficientBalanceException.class)
    public void shouldThrowExceptionWhenSenderBalanceNotSufficient() throws Exception {
        TransferDTO transferDto = new TransferDTO();
        SenderDTO sender = new SenderDTO();
        sender.setUserId(1l);
        sender.setBankAccountId(1L);
        transferDto.setSender(sender);

        AmountDTO amount = new AmountDTO();
        amount.setAmount(new BigDecimal(6));
        amount.setCurrency(Currency.EUR);
        transferDto.setAmount(amount);

        UserEntity senderEntity = new UserEntity();
        senderEntity.setBlacklisted(false);


        RecipientDTO recipient = new RecipientDTO();
        recipient.setUserId(2L);
        transferDto.setRecipient(recipient);

        UserEntity recipientEntity = new UserEntity();
        recipientEntity.setBlacklisted(false);

        BankAccountEntity senderAccount = new BankAccountEntity();
        senderAccount.setBalance(new BigDecimal(5));
        senderAccount.setCurrency(Currency.EUR);


        when(userRepository.findById(1L)).thenReturn(senderEntity);
        when(userRepository.findById(2L)).thenReturn(recipientEntity);
        when(bankAccountRepository.findById(1L)).thenReturn(senderAccount);
        doThrow(new InsufficientBalanceException()).when(transactionValidationService).validateSufficientAmount(senderAccount, transferDto);


        moneyTransferService.transferMoney(transferDto);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowExceptionWhenRecipientBankAccountNotProvided() throws Exception {
        TransferDTO transferDto = new TransferDTO();
        SenderDTO sender = new SenderDTO();
        sender.setUserId(1l);
        transferDto.setSender(sender);

        UserEntity senderEntity = new UserEntity();
        senderEntity.setBlacklisted(false);

        RecipientDTO recipient = new RecipientDTO();
        recipient.setUserId(2L);
        transferDto.setRecipient(recipient);

        UserEntity recipientEntity = new UserEntity();
        recipientEntity.setBlacklisted(false);

        BankAccountEntity senderAccount = new BankAccountEntity();
        senderAccount.setBalance(new BigDecimal(5));
        senderAccount.setCurrency(Currency.EUR);

        when(userRepository.findById(1L)).thenReturn(senderEntity);
        when(userRepository.findById(2L)).thenReturn(recipientEntity);
        when(bankAccountRepository.findById(1L)).thenReturn(senderAccount);

        moneyTransferService.transferMoney(transferDto);
    }

    @Test
    public void shouldSuccessfullyTransferMoneyWithoutConversionWhenCurrencyMatches() throws Exception {
        TransferDTO transferDto = new TransferDTO();
        SenderDTO sender = new SenderDTO();
        sender.setUserId(1l);
        sender.setBankAccountId(1L);
        transferDto.setSender(sender);

        AmountDTO amount = new AmountDTO();
        amount.setAmount(new BigDecimal(3));
        amount.setCurrency(Currency.EUR);
        transferDto.setAmount(amount);

        UserEntity senderEntity = new UserEntity();
        senderEntity.setBlacklisted(false);

        RecipientDTO recipient = new RecipientDTO();
        recipient.setUserId(2L);
        recipient.setBankAccountId(2L);
        transferDto.setRecipient(recipient);

        UserEntity recipientEntity = new UserEntity();
        recipientEntity.setBlacklisted(false);

        BankAccountEntity senderAccount = new BankAccountEntity();
        senderAccount.setBalance(new BigDecimal(7));
        senderAccount.setCurrency(Currency.EUR);


        BankAccountEntity recipientAccount = new BankAccountEntity();
        recipientAccount.setBalance(new BigDecimal(2));
        recipientAccount.setCurrency(Currency.EUR);

        when(userRepository.findById(1L)).thenReturn(senderEntity);
        when(userRepository.findById(2L)).thenReturn(recipientEntity);
        when(bankAccountRepository.findById(1L)).thenReturn(senderAccount);
        when(bankAccountRepository.findById(2L)).thenReturn(recipientAccount);

        moneyTransferService.transferMoney(transferDto);

        verify(userRepository).findById(1L);
        verify(userRepository).findById(2L);
        verify(bankAccountRepository).findById(1L);
        verify(bankAccountRepository).save(senderAccount);
        verify(bankAccountRepository).save(recipientAccount);
        verify(transferRepository).save(any(TransferEntity.class));

        assertEquals(new BigDecimal(4), senderAccount.getBalance());
        assertEquals(new BigDecimal(5), recipientAccount.getBalance());
    }

    @Test
    public void shouldSuccessfullyTransferMoneyWithConversionWhenCurrencyMatches() throws Exception {
        TransferDTO transferDto = new TransferDTO();
        SenderDTO sender = new SenderDTO();
        sender.setUserId(1l);
        sender.setBankAccountId(1L);
        transferDto.setSender(sender);

        AmountDTO amount = new AmountDTO();
        amount.setAmount(new BigDecimal(3));
        amount.setCurrency(Currency.EUR);
        transferDto.setAmount(amount);

        UserEntity senderEntity = new UserEntity();
        senderEntity.setBlacklisted(false);

        RecipientDTO recipient = new RecipientDTO();
        recipient.setUserId(2L);
        recipient.setBankAccountId(2L);
        transferDto.setRecipient(recipient);

        UserEntity recipientEntity = new UserEntity();
        recipientEntity.setBlacklisted(false);

        BankAccountEntity senderAccount = new BankAccountEntity();
        senderAccount.setBalance(new BigDecimal(7));
        senderAccount.setCurrency(Currency.EUR);


        BankAccountEntity recipientAccount = new BankAccountEntity();
        recipientAccount.setBalance(new BigDecimal(2));
        recipientAccount.setCurrency(Currency.GBP);

        when(userRepository.findById(1L)).thenReturn(senderEntity);
        when(userRepository.findById(2L)).thenReturn(recipientEntity);
        when(bankAccountRepository.findById(1L)).thenReturn(senderAccount);
        when(bankAccountRepository.findById(2L)).thenReturn(recipientAccount);
        when(currencyConversionService.convertTo(new BigDecimal(3), Currency.EUR, Currency.GBP)).thenReturn(new BigDecimal("2.67"));

        moneyTransferService.transferMoney(transferDto);

        verify(userRepository).findById(1L);
        verify(userRepository).findById(2L);
        verify(bankAccountRepository).findById(1L);
        verify(bankAccountRepository).save(senderAccount);
        verify(bankAccountRepository).save(recipientAccount);
        verify(transferRepository).save(any(TransferEntity.class));

        assertEquals(new BigDecimal(4), senderAccount.getBalance());
        assertEquals(new BigDecimal("4.67"), recipientAccount.getBalance());
    }

}