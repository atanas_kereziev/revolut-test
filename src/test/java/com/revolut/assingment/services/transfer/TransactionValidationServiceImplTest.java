package com.revolut.assingment.services.transfer;

import com.revolut.assingment.dto.transfer.AmountDTO;
import com.revolut.assingment.dto.transfer.TransferDTO;
import com.revolut.assingment.exceptions.InsufficientBalanceException;
import com.revolut.assingment.exceptions.UserBlacklistedException;
import com.revolut.assingment.exceptions.ValidationException;
import com.revolut.assingment.model.common.Currency;
import com.revolut.assingment.model.transfer.BankAccountEntity;
import com.revolut.assingment.model.user.UserEntity;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class TransactionValidationServiceImplTest {

    private TransactionValidationServiceImpl transactionValidationService;

    @Before
    public void setUp() throws Exception {
        transactionValidationService = new TransactionValidationServiceImpl();
    }

    @Test(expected = UserBlacklistedException.class)
    public void shouldThrowUserBlacklistedExceptionWhenUserIsBlacklisted() throws Exception {

        UserEntity user = new UserEntity();
        user.setBlacklisted(true);

        transactionValidationService.validateUserNotBlacklisted(user);
    }

    @Test
    public void shouldSuccessfullyValidateUserWhenNotBlacklisted() throws Exception {

        UserEntity user = new UserEntity();

        transactionValidationService.validateUserNotBlacklisted(user);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowValidationExceptionWhenAmountNotProvidedOnValidateSufficientAmount() throws Exception {

        BankAccountEntity account = new BankAccountEntity();
        TransferDTO transferDto = new TransferDTO();
        transactionValidationService.validateSufficientAmount(account, transferDto);
    }

    @Test(expected = InsufficientBalanceException.class)
    public void shouldThrowInsufficientBalanceExceptionWhenAmountNotSufficient() throws Exception {

        BankAccountEntity account = new BankAccountEntity();
        account.setBalance(new BigDecimal("4.00"));

        TransferDTO transferDto = new TransferDTO();
        AmountDTO amount = new AmountDTO();
        amount.setAmount(new BigDecimal("5.00"));
        amount.setCurrency(Currency.EUR);

        transferDto.setAmount(amount);
        transactionValidationService.validateSufficientAmount(account, transferDto);
    }

    @Test
    public void shouldSuccessfullyValidateBalanceWhenAmountIsSufficient() throws Exception {

        BankAccountEntity account = new BankAccountEntity();
        account.setBalance(new BigDecimal("5.00"));

        TransferDTO transferDto = new TransferDTO();
        AmountDTO amount = new AmountDTO();
        amount.setAmount(new BigDecimal("5.00"));
        amount.setCurrency(Currency.EUR);

        transferDto.setAmount(amount);
        transactionValidationService.validateSufficientAmount(account, transferDto);
    }
}