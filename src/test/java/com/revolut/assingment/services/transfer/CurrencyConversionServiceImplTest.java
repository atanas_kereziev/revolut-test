package com.revolut.assingment.services.transfer;


import com.revolut.assingment.model.common.Currency;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class CurrencyConversionServiceImplTest {

    private CurrencyConversionServiceImpl currencyConversionService;

    @Before
    public void setUp() throws Exception {
        currencyConversionService = new CurrencyConversionServiceImpl();
    }

    @Test
    public void shouldConvertEurToGbp() throws Exception {
        BigDecimal initialAmount = BigDecimal.ONE;
        BigDecimal result = currencyConversionService.convertTo(initialAmount, Currency.EUR, Currency.GBP);

        assertEquals(new BigDecimal("0.89"), result);
    }
}