package com.revolut.assingment.services.transfer;


import com.revolut.assingment.model.common.Currency;

import java.math.BigDecimal;

public class CurrencyConversionServiceImpl implements CurrencyConversionService {

    private static final BigDecimal EUR_TO_GBP_RATE = new BigDecimal("0.89");

    public BigDecimal convertTo(BigDecimal initialAmount, Currency initialCurrency, Currency resultCurrency) {
        // For simplicity our conversion service will support only EUR to GBP conversion
        BigDecimal convertedValue = initialAmount.multiply(EUR_TO_GBP_RATE);
        return convertedValue;
    }
}
