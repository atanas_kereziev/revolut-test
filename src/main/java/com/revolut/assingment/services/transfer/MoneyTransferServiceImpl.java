package com.revolut.assingment.services.transfer;


import com.revolut.assingment.dto.transfer.AmountDTO;
import com.revolut.assingment.dto.transfer.RecipientDTO;
import com.revolut.assingment.dto.transfer.SenderDTO;
import com.revolut.assingment.dto.transfer.TransferDTO;
import com.revolut.assingment.exceptions.ValidationException;
import com.revolut.assingment.injector.ComponentCreator;
import com.revolut.assingment.model.common.Currency;
import com.revolut.assingment.model.transfer.BankAccountEntity;
import com.revolut.assingment.model.transfer.TransferEntity;
import com.revolut.assingment.model.user.UserEntity;
import com.revolut.assingment.repository.BankAccountRepository;
import com.revolut.assingment.repository.TransferRepository;
import com.revolut.assingment.repository.UserRepository;

import java.math.BigDecimal;
import java.util.Random;

public class MoneyTransferServiceImpl implements MoneyTransferService {

    private UserRepository userRepository;
    private BankAccountRepository bankAccountRepository;
    private TransferRepository transferRepository;
    private CurrencyConversionService currencyConversionService;
    private TransactionValidationService transactionValidationService;

    public MoneyTransferServiceImpl(UserRepository userRepository, BankAccountRepository bankAccountRepository,
                                    CurrencyConversionService currencyConversionService,
                                    TransactionValidationService transactionValidationService, TransferRepository transferRepository) {
        this.userRepository = userRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.currencyConversionService = currencyConversionService;
        this.transactionValidationService = transactionValidationService;
        this.transferRepository = transferRepository;
    }

    public MoneyTransferServiceImpl() {
        init();
        insertTestData();
    }

    private void init() {
        userRepository = ComponentCreator.getUserRepositoryBean();
        bankAccountRepository = ComponentCreator.getBankAccountRepositoryBean();
        transferRepository = ComponentCreator.getTransferRepositoryBean();
        currencyConversionService = ComponentCreator.getCurrencyConversionServiceBean();
        transactionValidationService = ComponentCreator.getTransactionValidationServiceBean();
    }

    public AmountDTO transferMoney(TransferDTO transferDTO) {
        if (transferDTO == null) {
            throw new ValidationException();
        }
        UserEntity sender = extractSender(transferDTO);
        transactionValidationService.validateUserNotBlacklisted(sender);

        UserEntity recipient = extractRecipient(transferDTO);
        transactionValidationService.validateUserNotBlacklisted(recipient);

        BankAccountEntity senderAccount = extractSenderBankAccount(transferDTO);
        transactionValidationService.validateSufficientAmount(senderAccount, transferDTO);

        BankAccountEntity recipientAccount = extractRecipientBankAccount(transferDTO);

        return executeTransaction(senderAccount, recipientAccount, transferDTO);
    }

    private AmountDTO executeTransaction(BankAccountEntity senderAccount, BankAccountEntity recipientAccount, TransferDTO transferDTO) {
        AmountDTO amount = transferDTO.getAmount();
        BigDecimal senderBalance = senderAccount.getBalance();
        BigDecimal newSenderBalance = senderBalance.subtract(amount.getAmount());
        senderAccount.setBalance(newSenderBalance);
        BigDecimal newRecipientBalance;
        if (!isMatchingCurrency(senderAccount, recipientAccount)) {
            BigDecimal convertedAmount = currencyConversionService.convertTo(amount.getAmount(), amount.getCurrency(), recipientAccount.getCurrency());
            newRecipientBalance = recipientAccount.getBalance().add(convertedAmount);
        } else {
            newRecipientBalance = recipientAccount.getBalance().add(amount.getAmount());
        }
        recipientAccount.setBalance(newRecipientBalance);

        bankAccountRepository.save(senderAccount);
        bankAccountRepository.save(recipientAccount);

        TransferEntity transferEntity = constructTransferEntity(transferDTO);
        transferRepository.save(transferEntity);
        return amount;
    }

    private TransferEntity constructTransferEntity(TransferDTO transferDTO) {
        TransferEntity transferEntity = new TransferEntity();
        transferEntity.setAmount(transferDTO.getAmount().getAmount());
        transferEntity.setCurrency(transferDTO.getAmount().getCurrency());
        Random random = new Random(1000);
        transferEntity.setId(random.nextLong());
        transferEntity.setReason(transferDTO.getReason());
        transferEntity.setSenderId(transferDTO.getSender().getUserId());
        transferEntity.setRecipientId(transferDTO.getRecipient().getUserId());

        return transferEntity;
    }

    private UserEntity extractSender(TransferDTO transferDTO) {
        SenderDTO sender = transferDTO.getSender();
        if (sender == null || sender.getUserId() == null) {
            throw new ValidationException();
        }
        return userRepository.findById(sender.getUserId());
    }

    private UserEntity extractRecipient(TransferDTO transferDTO) {
        RecipientDTO recipient = transferDTO.getRecipient();
        if (recipient == null || recipient.getUserId() == null) {
            throw new ValidationException();
        }
        return userRepository.findById(recipient.getUserId());
    }

    private BankAccountEntity extractSenderBankAccount(TransferDTO transferDTO) {
        Long accountId = transferDTO.getSender().getBankAccountId();
        if (accountId == null) {
            throw new ValidationException();
        }
        return bankAccountRepository.findById(accountId);
    }
    private BankAccountEntity extractRecipientBankAccount(TransferDTO transferDTO) {
        Long accountId = transferDTO.getRecipient().getBankAccountId();
        if (accountId == null) {
            throw new ValidationException();
        }
        return bankAccountRepository.findById(accountId);
    }

    private boolean isMatchingCurrency(BankAccountEntity senderAccount, BankAccountEntity recipientAccount) {
        return senderAccount.getCurrency().equals(recipientAccount.getCurrency());
    }

    private void insertTestData() {
        UserEntity user1 = new UserEntity();
        user1.setId(1L);
        user1.setFistName("Tester 1");
        user1.setLastName("Tester 1 last name");

        UserEntity user2 = new UserEntity();
        user2.setId(2L);
        user2.setFistName("Tester 2");
        user2.setLastName("Tester 2 last name");

        userRepository.save(user1);
        userRepository.save(user2);

        BankAccountEntity user1Account = new BankAccountEntity();
        user1Account.setId(1L);
        user1Account.setBalance(new BigDecimal(7));
        user1Account.setCurrency(Currency.EUR);

        BankAccountEntity user2Account = new BankAccountEntity();
        user2Account.setId(2L);
        user2Account.setBalance(new BigDecimal(2));
        user2Account.setCurrency(Currency.EUR);

        bankAccountRepository.save(user1Account);
        bankAccountRepository.save(user2Account);
    }
}
