package com.revolut.assingment.services.transfer;


import com.revolut.assingment.dto.transfer.AmountDTO;
import com.revolut.assingment.dto.transfer.TransferDTO;

public interface MoneyTransferService {

    AmountDTO transferMoney(TransferDTO transferDTO);
}
