package com.revolut.assingment.services.transfer;


import com.revolut.assingment.dto.transfer.TransferDTO;
import com.revolut.assingment.model.transfer.BankAccountEntity;
import com.revolut.assingment.model.user.UserEntity;

public interface TransactionValidationService {

    void validateUserNotBlacklisted(UserEntity user);
    void validateSufficientAmount(BankAccountEntity account, TransferDTO transferDTO);
}
