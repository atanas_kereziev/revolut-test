package com.revolut.assingment.services.transfer;


import com.revolut.assingment.dto.transfer.AmountDTO;
import com.revolut.assingment.dto.transfer.TransferDTO;
import com.revolut.assingment.exceptions.InsufficientBalanceException;
import com.revolut.assingment.exceptions.UserBlacklistedException;
import com.revolut.assingment.exceptions.ValidationException;
import com.revolut.assingment.model.transfer.BankAccountEntity;
import com.revolut.assingment.model.user.UserEntity;

import java.math.BigDecimal;

public class TransactionValidationServiceImpl implements TransactionValidationService {

    public void validateUserNotBlacklisted(UserEntity user) {
        if (user.isBlacklisted()) {
            throw new UserBlacklistedException();
        }
    }

    public void validateSufficientAmount(BankAccountEntity account, TransferDTO transferDTO) {
        AmountDTO amount = transferDTO.getAmount();
        if (amount == null || amount.getAmount() == null) {
            throw new ValidationException();
        }
        BigDecimal transferableAmount = amount.getAmount();
        BigDecimal accountBalance = account.getBalance();
        if (transferableAmount.compareTo(accountBalance) == 1) {
            throw new InsufficientBalanceException();
        }
    }
}
