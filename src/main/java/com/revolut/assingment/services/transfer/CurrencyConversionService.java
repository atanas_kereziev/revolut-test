package com.revolut.assingment.services.transfer;


import com.revolut.assingment.dto.transfer.AmountDTO;
import com.revolut.assingment.model.common.Currency;

import java.math.BigDecimal;

public interface CurrencyConversionService {

    BigDecimal convertTo(BigDecimal initialAmount, Currency initialCurrency, Currency resultCurrency);
}
