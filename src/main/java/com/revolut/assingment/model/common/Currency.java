package com.revolut.assingment.model.common;


public enum Currency {

    EUR, GBP, USD
}
