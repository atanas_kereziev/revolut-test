package com.revolut.assingment.repository;


import com.revolut.assingment.model.user.UserEntity;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UserRepositoryImpl implements UserRepository {

    private static Map<Long, UserEntity> userStore = new ConcurrentHashMap<Long, UserEntity>();

    public UserEntity findById(Long id) {
        return userStore.get(id);
    }

    public UserEntity save(UserEntity user) {
        return userStore.put(user.getId(), user);
    }
}
