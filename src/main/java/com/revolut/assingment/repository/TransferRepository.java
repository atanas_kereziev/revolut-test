package com.revolut.assingment.repository;


import com.revolut.assingment.model.transfer.TransferEntity;

public interface TransferRepository {

    TransferEntity findById(Long id);
    TransferEntity save(TransferEntity entity);

}
