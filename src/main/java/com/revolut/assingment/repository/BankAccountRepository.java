package com.revolut.assingment.repository;


import com.revolut.assingment.model.transfer.BankAccountEntity;

public interface BankAccountRepository {

    BankAccountEntity findById(Long id);

    BankAccountEntity save(BankAccountEntity entity);
}
