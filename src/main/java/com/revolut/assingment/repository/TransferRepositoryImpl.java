package com.revolut.assingment.repository;


import com.revolut.assingment.model.transfer.TransferEntity;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TransferRepositoryImpl implements TransferRepository {

    private static Map<Long, TransferEntity> transferStore = new ConcurrentHashMap<>();


    public TransferEntity findById(Long id) {
        return transferStore.get(id);
    }

    public TransferEntity save(TransferEntity entity) {
        return transferStore.put(entity.getId(), entity);
    }
}
