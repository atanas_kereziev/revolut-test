package com.revolut.assingment.repository;


import com.revolut.assingment.model.transfer.BankAccountEntity;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BankAccountRepositoryImpl implements BankAccountRepository {

    private static Map<Long, BankAccountEntity> accountStore = new ConcurrentHashMap<>();


    public BankAccountEntity findById(Long id) {
        return accountStore.get(id);
    }

    public BankAccountEntity save(BankAccountEntity entity) {
        return accountStore.put(entity.getId(), entity);
    }
}
