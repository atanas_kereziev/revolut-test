package com.revolut.assingment.repository;


import com.revolut.assingment.model.user.UserEntity;

public interface UserRepository {

    UserEntity findById(Long id);

    UserEntity save(UserEntity user);

}
