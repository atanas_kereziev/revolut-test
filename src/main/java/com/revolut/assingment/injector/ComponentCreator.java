package com.revolut.assingment.injector;


import com.revolut.assingment.repository.*;
import com.revolut.assingment.services.transfer.*;

// As we do not use any Dependency Injection Container this class will serve as a simple DI container.
public class ComponentCreator {

    private static UserRepository userRepository;
    private static BankAccountRepository bankAccountRepository;
    private static TransferRepository transferRepository;
    private static CurrencyConversionService currencyConversionService;
    private static TransactionValidationService transactionValidationService;
    private static MoneyTransferService moneyTransferService;

    public static UserRepository getUserRepositoryBean() {
        if (userRepository == null) {
            userRepository = new UserRepositoryImpl();
        }
        return userRepository;
    }

    public static BankAccountRepository getBankAccountRepositoryBean() {
        if (bankAccountRepository == null) {
            bankAccountRepository = new BankAccountRepositoryImpl();
        }
        return bankAccountRepository;
    }

    public static TransferRepository getTransferRepositoryBean() {
        if (transferRepository == null) {
            transferRepository = new TransferRepositoryImpl();
        }
        return transferRepository;
    }

    public static CurrencyConversionService getCurrencyConversionServiceBean() {
        if (currencyConversionService == null) {
            currencyConversionService = new CurrencyConversionServiceImpl();
        }
        return currencyConversionService;
    }

    public static TransactionValidationService getTransactionValidationServiceBean() {
        if(transactionValidationService == null) {
            transactionValidationService = new TransactionValidationServiceImpl();
        }
        return transactionValidationService;
    }

    public static MoneyTransferService getMoneyTransferServiceBean() {
        if (moneyTransferService == null) {
            moneyTransferService = new MoneyTransferServiceImpl();
        }
        return moneyTransferService;
    }
}
