package com.revolut.assingment.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.revolut.assingment.dto.ErrorDTO;
import com.revolut.assingment.dto.transfer.AmountDTO;
import com.revolut.assingment.dto.transfer.TransferDTO;
import com.revolut.assingment.exceptions.InsufficientBalanceException;
import com.revolut.assingment.exceptions.UserBlacklistedException;
import com.revolut.assingment.exceptions.ValidationException;
import com.revolut.assingment.injector.ComponentCreator;
import com.revolut.assingment.services.transfer.MoneyTransferService;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/money/transfer"})
public class MoneyTransferServlet extends HttpServlet {

    private MoneyTransferService moneyTransferService = ComponentCreator.getMoneyTransferServiceBean();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TransferDTO transferDTO = getTransferDto(request);

        try {
            AmountDTO result = moneyTransferService.transferMoney(transferDTO);
            addResponsePayload(response, result);
        } catch (ValidationException | InsufficientBalanceException | UserBlacklistedException e) {
            handleException(response, e);
        }
    }

    private TransferDTO getTransferDto(HttpServletRequest request) throws IOException {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(request.getReader(), TransferDTO.class);
    }

    private void addResponsePayload(HttpServletResponse response, Object body) throws IOException {
        response.addHeader("Content-Type", "application/json");
        if (body == null) {
            return;
        }
        Gson gson = new GsonBuilder().create();
        String jsonBody = gson.toJson(body);

        ServletOutputStream out = response.getOutputStream();
        out.write(jsonBody.getBytes());
        out.flush();
        out.close();
    }

    private void handleException(HttpServletResponse response, RuntimeException e) throws IOException {
        ErrorDTO error = null;
        if (e instanceof ValidationException) {
            error = ErrorDTO.validationError();
        } else if (e instanceof InsufficientBalanceException) {
            error = ErrorDTO.insufficientBalanceError();
        } else if (e instanceof UserBlacklistedException) {
            error = ErrorDTO.userBlacklistedError();
        }
        response.setStatus(412);
        addResponsePayload(response, error);
    }
}
