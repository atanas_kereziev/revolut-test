package com.revolut.assingment.dto;


public class ErrorDTO {

    private int code;
    private String message;

    public ErrorDTO (int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static ErrorDTO validationError() {
        return new ErrorDTO(101, "Validation error");
    }

    public static ErrorDTO insufficientBalanceError() {
        return new ErrorDTO(102, "Insufficient balance error");
    }

    public static ErrorDTO userBlacklistedError() {
        return new ErrorDTO(103, "User is blacklisted");
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
